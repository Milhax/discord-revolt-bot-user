from enum import Enum
import os
import readchar

# ADD RETURNS

Input = Enum('Input', ["UP", "DOWN", "ENTER", "INVALID"])

class ansi():
    @staticmethod
    def set_bg(r: int, g: int, b: int) -> None:
        print('\x1b[48;2;' + str(r) + ';' + str(g) + ';' + str(b) + 'm', end='')
    @staticmethod
    def set_fg(r: int, g: int, b: int) -> None:
        print('\x1b[38;2;' + str(r) + ';' + str(g) + ';' + str(b) + 'm', end='')
    @staticmethod
    def bold() -> None:
        print('\x1b[1m', end='')
    @staticmethod
    def reset() -> None:
        print('\x1b[0m', end='')

class color():
    r: int = 0
    g: int = 0
    b: int = 0
    def set_fg(self):
        ansi.set_fg(self.r, self.g, self.b)
    def set_bg(self):
        ansi.set_bg(self.r, self.g, self.b)

class menu():
    select_fg: color = color()
    select_fg.r = 255

    options: list[tuple[str, str]] = []

    selected: int = 0

    def register(self, value: str, name: str):
        self.options.append((value, name))
    def _get_input(self) -> Input:
        key = readchar.readkey()
        if key == readchar.key.UP:
            return Input.UP
        if key == readchar.key.DOWN:
            return Input.DOWN
        if key == readchar.key.ENTER:
            return Input.ENTER
        else:
            return Input.INVALID
    def _clear(self):
        print('\033[1;1H\033[2J', end="")
    def _print(self):
        self._clear()
        height: int = os.get_terminal_size()[1]
        l: int = len(self.options)
        min_val: int = 0
        if l > height:
            min_val = self.selected - (height//2)
            if min_val < 0:
                min_val = 0
        max_val: int = min_val + height - 2
        if l == 0:
            return
        else:
            for i in range(l):
                if i < min_val:
                    continue
                if i > max_val:
                    continue
                if i == self.selected:
                    self.select_fg.set_fg()
                    ansi.bold()
                print(self.options[i][1])
                if i == self.selected:
                    ansi.reset()
    def run(self) -> str:
        while True:
            self._print()
            inp = self._get_input()
            if inp == Input.UP:
                self.selected -= 1
                self.selected %= len(self.options)
            if inp == Input.DOWN:
                self.selected += 1
                self.selected %= len(self.options)
            if inp == Input.ENTER:
                return self.options[self.selected][0]

if __name__ == "__main__":
    a = menu()
    for i in range(30):
        a.register("opt" + str(i), "option " + str(i))
    print(a.run())
